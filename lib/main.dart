import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lecture_practice_4/adapter.dart';

import 'factory.dart';

void main() => runApp(
      Application(),
    );

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MyRect(10, 10, 20, 20).render();
    return MaterialApp(
      theme: ThemeData.dark(),
      home: MyPage(),
    );
  }
}

class MyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AndroidAlertDialog().create(context);
    return Scaffold(
      body: Column(
        children: <Widget>[

        ],
      ),
    );
  }
}

