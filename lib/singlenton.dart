class Counter {
  int i = 0;

  static Counter instance = Counter();

  void increment() => i++;
  void decrement() => i--;
}
