
abstract class Switch {
  Appliance appliance;
  void turnOn();
}

abstract class Appliance {
  void run();
}

class RemoteControl extends Switch {
  Appliance appliance;

  RemoteControl({this.appliance});

  @override
  void turnOn() {
    this.appliance.run();
  }
}

class TV extends Appliance {
  @override
  void run() {
    print("tv turned on");
  }
}

class VacuumClearner extends Appliance {
  @override
  void run() {
    print("vacuum cleaner turned on");
  }
}

main(List<String> arguments) {
  final tvRemoteControl = new RemoteControl(appliance: new TV());
  tvRemoteControl.turnOn();

  final vacuumRemoteControl = new RemoteControl(appliance: new VacuumClearner());
  vacuumRemoteControl.turnOn();
}